﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerGame2 : MonoBehaviour
{
    [SerializeField] private BossSpaceship bossSpaceship;
    [SerializeField] private int bossSpaceshipHp;
    [SerializeField] private int bossSpaceshipMoveSpeed;

    void Awake()
    {
        Debug.Assert(bossSpaceship != null, "enemySpaceship cannot be null");
        Debug.Assert(bossSpaceshipHp > 0, "bossSpaceshipHp has to be more than zero");
        Debug.Assert(bossSpaceshipMoveSpeed > 0, "bossSpaceshipMoveSpeed has to be more than zero");
    }
    
    void Start()
    {
        SpawnBossSpaceship();
    }

    private void SpawnBossSpaceship()
    {
        var spawnedBossShip = Instantiate(bossSpaceship);
        spawnedBossShip.Init(bossSpaceshipHp, bossSpaceshipMoveSpeed);
        spawnedBossShip.OnExploded += OnBossSpaceshipExploded;
    }

    private void OnBossSpaceshipExploded()
    {
        SceneManager.LoadScene("Game3");
        
        SpawnBossSpaceship();
        DestroyRemainingShips();
    }
    
    private void DestroyRemainingShips()
    {
        var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in remainingEnemy)
        {
            Destroy(enemy);
        }
        var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in remainingPlayer)
        {
            Destroy(player);
        }            
    }
}
