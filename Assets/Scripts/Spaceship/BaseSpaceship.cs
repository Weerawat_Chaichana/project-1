using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected Bullet defaultBullet1;
        [SerializeField] protected Transform gunPosition1;
        [SerializeField] protected Bullet defaultBullet2;
        [SerializeField] protected Transform gunPosition2;

        protected AudioSource audioSource;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(defaultBullet1 != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition1 != null, "gunPosition cannot be null");
            Debug.Assert(defaultBullet2 != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition2 != null, "gunPosition cannot be null");
        }        
        
        protected void Init(int hp, float speed, Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }

        public abstract void Fire();
    }
}